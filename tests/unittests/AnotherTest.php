<?php
/**
 * Created by PhpStorm.
 * User: falkkuhnel
 * Date: 23.11.17
 * Time: 15:51
 */


class MockTest extends PHPUnit\Framework\TestCase
{
    public function testWithAMock() {
        $mock = \Mockery::mock('SomeClass', ['method1' => 'ret_val1']);
        $mock->shouldReceive('method1')->once()->andReturn('Honeybunny'); //->once() or ->twice() or ->times(num) or ->atLeast()->times(1);
        $this->assertEquals("Honeybunny", $mock->method1());
    }

    public function testWithAnotherMock() {

        $mock = \Mockery::mock('SomeClass', ['method1' => 'ret_val1']);
        $mock->shouldReceive('method1')->times(3)->with(\Mockery::type('numeric'))->andReturn('Zonk', 'Honeybunny');
        $this->assertEquals("Zonk", $mock->method1(3));
        $mock->method1(30);
        $this->assertEquals("Honeybunny", $mock->method1(7698768));
    }

    public function tearDown() {
        \Mockery::close();
    }
}
