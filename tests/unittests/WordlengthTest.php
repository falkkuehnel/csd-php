<?php

class Wordlength
{
    private function getWordLength(string $word) :int {
        return strlen($word);
    }

    public function getWordsByLength(string $text) :array {

        $words = $this->getWords($text);

        $result = [];
        foreach ($words as $word) {
            $word = trim($word);

            $length = $this->getWordLength($word);

            $result[$length][] = $word;
        }

        return $result;
    }

    private function getWords(string $text) :array
    {
        return explode(' ', trim($text));
    }
}


/**
 * Created by PhpStorm.
 * User: MJagla
 * Date: 28.11.2017
 * Time: 09:12
 */

class WordlengthTest extends PHPUnit\Framework\TestCase
{
    public function setUp() {
        $this->wordlength = new Wordlength();
    }

    public function testSingleWord() {
        $this->assertSame([4=>['tick']], $this->wordlength->getWordsByLength('tick'));
    }

    public function testMultipleWords() {
        $this->assertSame([4=>['tick'], 5=>['trick', 'track']], $this->wordlength->getWordsByLength('tick trick track'));
    }
}