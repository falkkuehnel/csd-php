<?php

use Craftsmen\Service\GameEngine;

class GameEngineTest extends PHPUnit\Framework\TestCase
{
    public function tearDown()
    {
        parent::tearDown();
        \Mockery::close();
    }

    public function testCreateGame()
    {
        $engine = new GameEngine();
        $game = $engine->startGame('username');
        $this->assertInstanceOf(\Craftsmen\Model\Game::class, $game);
    }

    public function testName()
    {
        $engine = new GameEngine();
        $game = $engine->startGame('username');
        $this->assertSame('username', $game->getUser()->getName());
    }

    public function testEntranceAvailable()
    {
        $engine = new GameEngine();
        $game = $engine->startGame('username');

        $this->assertInstanceOf(Craftsmen\Model\RoomInterface::class, $game->getRoom('entrance'));
    }

    public function testEntranceAvailableWithCoin()
    {
        $engine = new GameEngine();
        $game = $engine->startGame('username');
        $items = $game->getRoom('entrance')->getItems();

        $this->assertSame(1, count($items));
        $this->assertInstanceOf(Craftsmen\Model\CoinInterface::class, $items[0]);
    }
}
