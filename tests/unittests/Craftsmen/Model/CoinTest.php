<?php

use Craftsmen\Model\Coin;

class CoinTest extends \PHPUnit\Framework\TestCase
{
    public function testCoin()
    {
        $value = rand(5, 10);

        $coin = new Coin($value);

        $this->assertSame($value, $coin->getValue());
    }
}
