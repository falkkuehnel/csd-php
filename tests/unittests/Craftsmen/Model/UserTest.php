<?php

use Craftsmen\Model\Coin;
use Craftsmen\Model\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    const USERNAME = 'Horst';

    private $user;


    public function testUserCanHaveName()
    {
        $this->assertEquals(self::USERNAME, $this->user->getName());
    }

    public function testAddMoney()
    {
        $oldMoney = $this->user->getMoney();
        $coin = new Coin(3);

        $this->user->addMoney($coin);
        $this->assertSame($oldMoney+3, $this->user->getMoney());
    }

    /**
     * @return User|void
     */
    protected function setUp()
    {
        $user = new User(self::USERNAME);
        $this->user = $user;
    }
}
