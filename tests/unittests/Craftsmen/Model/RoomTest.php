<?php

class RoomTest extends \PHPUnit\Framework\TestCase
{
    public function testRoomCanHaveAName()
    {
        $roomName = 'Einganghalle';
        $room = new \Craftsmen\Model\Room($roomName, '');
        $this->assertEquals($roomName, $room->getName());
    }

    public function testRoomCanHaveADescription()
    {
        $roomDescription = 'Raum Beschreibung';
        $room = new \Craftsmen\Model\Room('', $roomDescription);
        $this->assertEquals($roomDescription, $room->getDescription());
    }

    public function testRoomCanHaveAnItem()
    {
        $item = $this->prophesize(
            \Craftsmen\Model\ItemInterface::class
        );
        $room = new \Craftsmen\Model\Room('', '');
        $room->addItem($item->reveal());
        $items = $room->getItems();
        $this->assertCount(1, $items);
        $this->assertSame($item->reveal(), $items[0]);
    }
}