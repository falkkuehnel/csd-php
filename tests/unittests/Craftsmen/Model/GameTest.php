<?php

use Craftsmen\Model\Game;

class GameTest extends PHPUnit\Framework\TestCase
{
    public function testGameExists() {
        $userMock = \Mockery::mock(\Craftsmen\Model\User::class, ['Horst']);

        $actual = new Game($userMock);
        $this->assertInstanceOf(Craftsmen\Model\Game::class, $actual);
    }

    public function testAddRoom()
    {
        $room = new \Craftsmen\Model\Room('Einganghalle', 'Das ist die Eingangshalle');

        $userMock = \Mockery::mock(\Craftsmen\Model\User::class, ['Horst']);

        $game = new Game($userMock);

        $game->addRoom('entry', $room);

        $this->assertSame($room, $game->getRoom('entry'));
    }
}
