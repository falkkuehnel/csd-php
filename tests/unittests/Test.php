<?php
/**
 * Created by PhpStorm.
 * User: falkkuhnel
 * Date: 23.11.17
 * Time: 15:51
 */


class Test extends PHPUnit\Framework\TestCase
{
    public function testSomething() {
        $this->assertArrayHasKey('foo', ['foo' => 'baz']);
    }

    public function testSomething2() {
        $this->assertArrayHasKey('bar', ['bar' => 'baz']);
    }
}
