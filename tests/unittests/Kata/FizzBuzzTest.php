<?php
/**
 * Created by PhpStorm.
 * User: MJagla
 * Date: 29.11.2017
 * Time: 09:13
 */

use Kata\FizzBuzz;

class FizzBuzzTest #extends PHPUnit\Framework\TestCase
{
    private $fizzBuzz;

    public function setUp() {
        $this->fizzBuzz = new FizzBuzz();
    }
    public function testIsDivisibleByThree() {
        $this->assertSame('fizz', $this->fizzBuzz->convert(3));
    }
    public function testIsNotDivisibleByThree() {
        $this->assertSame(1, $this->fizzBuzz->convert(1));
    }
    public function testIsDivisibleByFive() {
        $this->assertSame('buzz', $this->fizzBuzz->convert(5));
    }
    public function testIsDivisibleByThreeAndFive() {
        $this->assertSame('fizzbuzz', $this->fizzBuzz->convert(15));
    }
    public function testIsDivisibleByFourAndNotByThreeAndNotByFive() {
        $this->assertSame('zapp', $this->fizzBuzz->convert(4));
    }
    /*
    public function testIsDivisibleByFourAndByThreeAndNotByFive() {
        $this->assertSame('fizzzapp', $this->fizzBuzz->convert(12));
    }
    public function testIsDivisibleByFourAndNotByThreeAndByFive() {
        $this->assertSame('buzzzapp', $this->fizzBuzz->convert(20));
    }
    public function testIsDivisibleByFourAndByThreeAndByFive() {
        $this->assertSame('fizzbuzzzapp', $this->fizzBuzz->convert(60));
    }
    */
}