Feature: Entering my name
  As a player
  I want to be called by my name
  so that i feel more comfortable

  Scenario: Entering my name
    Given I am on the "startpage"
    When I log in as "Horst"
    Then I should see "Horst"

  Scenario: Entering special chars
    Given I am on the "startpage"
    When I log in as "<Horst>"
    Then I should see "Was ist passiert?"
    And I should see "<Horst>"

  Scenario: Displaying energy
    Given I am on the "startpage"
    When I log in as "Horst"
    Then I should see "Horst -- Lebensenergie: 100/100"
    And I should see "Horst,"

  Scenario: Displaying money
    Given I am on the "startpage"
    When I log in as "Horst"
    Then I should see "Geld: 0"

  Scenario: Enter command "umsehen"
    Given I am on the "startpage"
    When I log in as "<Horst>"
    Then I should see an "#command" element
    Then I fill in "command" with "umsehen"
    Then I press "Abschicken"
    Then I should see "Tür im Norden"

  Scenario: Enter illegal command
    Given I am on the "startpage"
    When I log in as "<Horst>"
    Then I should see an "#command" element
    Then I fill in "command" with "falscher command"
    Then I press "Abschicken"
    Then I should see "Befehl nicht gefunden."

  Scenario: See Coin
    Given I am on the "startpage"
    When I log in as "<Horst>"
    Then I should see an "#command" element
    Then I fill in "command" with "umsehen"
    Then I press "Abschicken"
    Then I should see "Eine Münze liegt auf dem Boden."

  Scenario: Entering start room and see the correct description
    Given I am on the "startpage"
    When I log in as "Horst"
    Then I should see "Horst" in the "#roomDescription" element
