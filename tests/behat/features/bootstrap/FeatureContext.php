<?php

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends \Behat\MinkExtension\Context\RawMinkContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given I am on the :arg1
     */
    public function iAmOnThe($arg1)
    {
        $this->visitPath('/');
    }

    /**
     * @When I log in as :userName
     */
    public function iLogInAs($userName)
    {
        $this->getSession()->getPage()->fillField('name', $userName);
        $this->getSession()->getPage()->pressButton('Abschicken');
        $this->assertSession()->addressEquals('/game');
    }
}