Feature: Start the game
  As a beginner
  I want to see the rules of the game
  so that i can start playing

  Scenario: Coming to the page
    Given I am on "/"
    Then I should see "Willkommen zum ultimativen Textadventure"

  Scenario: Request to enter my name
    Given I am on "/"
    Then I should see "Bitte gib Deinen Namen ein"
    And I should see an "#name" element
