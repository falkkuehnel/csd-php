FROM php:7-fpm
RUN apt-get update && \
    apt-get install -y \
      git
RUN apt-get install -y zlib1g-dev \
    && docker-php-ext-install zip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
RUN mkdir -p /var/www/composer/vendor
RUN mkdir -p /var/www/html/reports/
ENV PATH "/var/www/html/vendor/bin:$PATH"

WORKDIR /var/www/html/

# add our composer dependancies
ADD composer.json composer.lock ./

# install our dependancies without running scripts
# then remove composers cache to save space
RUN composer install --no-scripts --no-autoloader --prefer-dist -vvv

COPY . /var/www/html/
RUN composer dump-autoload --optimize

