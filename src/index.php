<?php

use Craftsmen\Controller\GameController;
use Craftsmen\Service\GameEngine;

require_once __DIR__ . '/autoload.php';

$app = new Silex\Application();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app['GameController'] = function() use ($app) {
    return new GameController($app, $app['request_stack']->getCurrentRequest());
};

$app['GameEngine'] = function() use ($app) {
    return new GameEngine();
};

$app->get('/', 'GameController:startPageAction');
$app->post('/login', 'GameController:loginAction');
$app->get('/game', 'GameController:gameAction');
$app->post('/game', 'GameController:gameAction');

$app->run();