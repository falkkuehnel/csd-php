<?php

namespace Kata;

class FizzBuzz
{
    public function convert($int)
    {
        if ($int % 3 == 0 && $int % 5 == 0) {
            return "fizzbuzz";
        } elseif ($int % 3 == 0) {
            return "fizz";
        } elseif ($int % 4 == 0) {
            return "";
        } elseif ($int % 5 == 0) {
            return "buzz";
        } else {
            return $int;
        }
    }
}
