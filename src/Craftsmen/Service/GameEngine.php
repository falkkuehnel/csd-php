<?php

namespace Craftsmen\Service;

use Craftsmen\Model\Coin;
use Craftsmen\Model\Game;
use Craftsmen\Model\Room;
use Craftsmen\Model\User;


class GameEngine
{
    public function startGame($userName)
    {
        $user = new User($userName);
        $game = new Game($user);
        $this->initiateRooms($game);
        return $game;
    }

    private function initiateRooms(Game $game)
    {
        $entranceRoom = new Room(
            'Eingang',
            'Du befindest Dich in einem schlichten sehr kleinen Raum. Es gibt wenig Licht und es müffelt ein wenig.'
        );
        $coin = new Coin(7);
        $entranceRoom->addItem($coin);
        $entranceRoom->setLookAroundDescription('Du siehst Dich im Raum um und entdeckst eine Tür im Norden.');
        $game->addRoom('entrance', $entranceRoom);
    }
}
