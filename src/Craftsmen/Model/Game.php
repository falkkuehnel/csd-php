<?php
namespace Craftsmen\Model;

class Game {

    private $user;

    private $rooms = [];

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return \Craftsmen\Model\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function addRoom(string $roomId, Room $room)
    {
        $this->rooms[$roomId] = $room;
    }

    public function getRoom(string $roomId) :Room
    {
        return $this->rooms[$roomId];
    }
}
