<?php

namespace Craftsmen\Model;

interface UserInterface
{
    public function getName() :string;

    public function getEnergy() :int;

    public function getMoney() :int;
}