<?php

namespace Craftsmen\Model;

class Coin implements ItemInterface, CoinInterface
{
    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getDescription(): string
    {
        return 'I am a coin';
    }

    public function getName(): string
    {
        return 'Gold coin';
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
