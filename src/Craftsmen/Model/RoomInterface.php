<?php

namespace Craftsmen\Model;

interface RoomInterface
{
    public function getName() :string;
    public function getDescription() :string;
}