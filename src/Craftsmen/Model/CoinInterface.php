<?php

namespace Craftsmen\Model;

interface CoinInterface
{
    public function getValue() : int;
}
