<?php

namespace Craftsmen\Model;

interface ItemInterface
{
    public function getDescription() : string;

    public function getName() : string;
}
