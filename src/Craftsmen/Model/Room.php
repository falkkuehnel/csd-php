<?php
namespace Craftsmen\Model;

class Room implements RoomInterface, ItemCollectionInterface
{
    private $name;
    private $description;
    private $lookAroundDescription;


    private $items = [];

    public function __construct(string $name, string $description)
    {
        $this->name = $name;
        $this->description = $description;
    }

    public function getName() :string
    {
        return $this->name;
    }

    public function getDescription() :string
    {
        return $this->description;
    }

    public function addItem(ItemInterface $item)
    {
        $this->items[] = $item;
    }

    public function getItems() :array
    {
        return $this->items;
    }

    public function setLookAroundDescription($lookAroundDescription)
    {
        $this->lookAroundDescription = $lookAroundDescription;
    }

    public function getLookAroundDescription()
    {
        return $this->lookAroundDescription . " Eine Münze liegt auf dem Boden.";
    }


}
