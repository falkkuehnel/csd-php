<?php

namespace Craftsmen\Model;

/**
 * @package Craftsmen\Model
 */
class User implements UserInterface
{
    private $name;
    private $energy;
    private $money;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->energy = 100;
        $this->money = 0;
    }

    public function getName() :string
    {
        return $this->name;
    }

    public function getEnergy() :int
    {
        return $this->energy;
    }

    public function getMoney() :int
    {
        return $this->money;
    }

    public function addMoney(CoinInterface $coin)
    {
        $this->money += $coin->getValue();
    }
}
