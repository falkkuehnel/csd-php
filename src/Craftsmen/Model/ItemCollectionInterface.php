<?php

namespace Craftsmen\Model;

interface ItemCollectionInterface
{
    public function addItem(ItemInterface $item);

    public function getItems() :array;
}
