<?php

namespace Craftsmen\Controller;

use Craftsmen\Model\Game;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class GameController
{
    private $app;
    private $twig;
    private $session;
    private $currentRequest;

    public function __construct(Application $app, Request $currentRequest)
    {
        $this->app = $app;
        $this->twig = $app['twig'];
        $this->session = $app['session'];
        $this->currentRequest = $currentRequest;
    }

    public function startPageAction()
    {
        return $this->twig->render('home.twig');
    }

    public function loginAction(Request $request)
    {
        $name = $request->get("name");
        if ($name !== null) {
            $engine = $this->app['GameEngine'];
            $game = $engine->startGame($name);
            $this->session->set('game', $game);
        } else {
            throw new \Exception('No name provided');
        }
        return $this->app->redirect('/game');
    }

    public function gameAction(Request $request)
    {
        /** @var Game $game */
        $game = $this->session->get('game');

        $command = $request->get("command");
        $parameters = ['user' => $game->getUser()];
        $parameters['description'] = $game->getRoom('entrance')->getDescription();
        $parameters['subtemplate'] = '';
        if ($command !== null) {
            if ($command == "umsehen") {
                $parameters['lookAroundDescription'] = $game->getRoom('entrance')->getLookAroundDescription();
                $parameters['subtemplate'] = "umsehen";
            } else {
                $parameters['subtemplate'] = "error";
            }
        }
        return $this->twig->render('rooms/startingRoom.twig', $parameters);
    }
}
