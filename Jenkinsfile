pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                sh '/usr/local/bin/docker build -t csd-php .'
                sh '/usr/local/bin/docker-compose -f docker-compose.yml -f docker-compose-jenkins.yml up -d'
            }
        }
        stage('UnitTests') {
            steps {
                sh '/usr/local/bin/docker-compose exec -T php phpunit -c ./config/phpunit.xml --log-junit /var/www/html/reports/phpunit/phpunit.xml'
            }
            post {
                always {
                    junit '**/reports/phpunit/*.xml'
                }
            }
        }
        stage('Acceptance Tests') {
            steps {
                sh '/usr/local/bin/docker-compose exec -T php behat -c ./tests/behat/behat.yml -f junit -o /var/www/html/reports/behat'
            }
            post {
                always {
                    junit '**/reports/behat/*.xml'
                }
            }
        }
        stage('Deploy') {
            when {
              expression {
                currentBuild.result == null || currentBuild.result == 'SUCCESS'
              }
            }
            steps {
                echo 'Publishing...'
                sh '/usr/local/bin/docker tag csd-php:latest csd-staging:latest'
                sh '/usr/local/bin/docker-compose -p staging -f docker-compose.yml -f docker-compose-staging.yml up -d'
            }
        }

    }
    post {
        always {
            sh '/usr/local/bin/docker-compose down'
        }
    }
}