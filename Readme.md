#Agile Craftsmenship

##Requirements
- Bitbucket Account
- docker
    - setup your docker to use this proxy: 
        - Linux: /etc/docker/daemon.json
        - Windows: %programdata%\docker\config\daemon.json
        -   Mac: Preferences -> Daemon -> Advanced

```
  "registry-mirrors" : [
    "http://10.74.12.12:5000"
  ],
  "insecure-registries" : [
    "10.74.12.12:5000"
  ],
```

#### Build the php application
`docker build -t csd-php .`

#### installing composer files for development
`docker-compose run --rm php composer install`

### Run Tests
`docker-compose run --rm php ./runTest.sh`

#### Running only Unittests
`docker-compose run --rm php phpunit -c ./config/phpunit.xml`

#### Running only Acceptancetests
`docker-compose run --rm php behat -c ./tests/behat/behat.yml`

